'use strict'
let a = new Promise(
	(resolve, reject) => {
		setTimeout(() => {
			let a = 100;
			let b = 200;
			let c = 'epic fail!'
			if ( Math.floor( Math.random() +0.5 ) ){
				resolve({a:a,b:b}); // переведёт промис в состояние fulfilled с результатом "result"
			} else {
				reject(c)
			}
    	}, 1);
	}
)
a.then (
	res => {
		console.log(res.a + res.b)
	}
).catch(err => {
		console.log(err)
	}
)
