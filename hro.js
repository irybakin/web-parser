'use strict'

const puppeteer = require('puppeteer');
const fs = require('fs')

let inputs = fs.readFileSync('./reports.txt').toString().split(/\r?\n/)
inputs = inputs.map((str) => {
	str = str.split('\t')
	return {
		url : str[0],
		sample : str[1],
		size : str[2]
	}
})


function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function write(fs, data) {
    return new Promise(resolve => fs.write(data, resolve));
}

function hash(url) {
	var regexp = new RegExp('\/([^/\n]+)$')
	var result = regexp.exec(url)
	return result[1].replace('\?','')
}

let login = process.argv[2];
let password = process.argv[3];
let d = new Date()
let res_dir = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+'-'+d.getHours()+'-'+d.getMinutes()+'-'+d.getSeconds()+'_results';

fs.mkdirSync('./' + res_dir)
let log = fs.createWriteStream('./'+res_dir+'/log.txt')

log.write('login/password are passed: ' + login + '/' + password+'\n')

puppeteer.launch().then(async browser => {
	const page = await browser.newPage();
	page.setViewport({width: 1600, height: 1200});
	await page.goto('http://analytics.hro.ru/site/login');
	await page.evaluate(
		function(login, password) {
			document.getElementById('LoginForm_username').value = login;
			document.getElementById('LoginForm_password').value = password;
			$('button').click();
		}, login, password
	);
	await page.screenshot({path: './'+res_dir+'/loginpage.png'});

	//inputs = inputs.slice(0,2);
	let result = [];
	async function nexp_page () {
		let input = inputs.pop();
		if (input) {
			let start_time = Date.now();
			const page = await browser.newPage();
			await page.goto(input.url,{ waitUntil : 'networkidle', networkIdleInflight: 2 , networkIdleTimeout : 2000} );
			let end_time = Date.now();
			page.setViewport({width: 1600, height: 1200});
			await timeout(2000);
			await page.screenshot( {path: './'+res_dir+'/' + hash(input.url) + '.png'} )
			
			let frames = await page.frames();
			await write(log, 'Quantity of frames ' + frames.length + '\n')

			//Рекурсивный вывод дерева фреймов
			await write(log, 'Frames tree:' + '\n')

			let str = [];
			dumpFrameTree(page.mainFrame(), '');
			function dumpFrameTree(frame, indent) {
				str.push(indent + frame.url())
				for (let child of frame.childFrames())
					dumpFrameTree(child, indent + '\t');
			}
			await write(log, str.join('\n') + '\n');
			let content = [];
			async function chain() {
				let f = frames.pop();
				if (f) {
					content.push(await f.evaluate(() => {
							return document.getElementsByTagName("body")[0].innerText;
						})
					)
					await chain();
				}
			}
			await chain();
			content = content.map((text) => {
				return 'length: '+ text.length + '\n'+ text
			})
			fs.writeFileSync('./'+res_dir+'/' + hash(input.url) + '.txt', content.join('\n###\n') + '\n')
			await page.close();
			await write(log, str.join('\n') + '\n');	
			result.push({
				loaded : content_analyze(content, input),
				time : end_time - start_time,
				url : input.url
			}) 
			await nexp_page();
		}
	}
	await nexp_page();
	console.log(JSON.stringify(result))
	await browser.close();
});

function content_analyze(content, model) {
	let allowance = 5;

	function test(frame, model) {
		//console.log((1 - Math.abs(frame.length - model.size) / model.size))

		if ( frame.search(RegExp(model.sample)) !== -1 && ( (Math.abs(frame.length - model.size) / model.size) < allowance/100 ) ) {
			return true
		} else {
			return false
		}
	}
	
	let mark = content.reduce((memo, frame) => {
		let res = test(frame, model)
		if( res ) {
			memo = memo || res
		}
		return memo
	}, false)
	return mark;
}

